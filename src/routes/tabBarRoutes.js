import React, { Component } from "react";
import { createBottomTabNavigator } from "react-navigation";

//Icons
import Icon from "react-native-vector-icons/FontAwesome5";

//screens
import Inicio from "../assets/screens/tabBar/Inicio";
import Beneficios from "../assets/screens/tabBar/Beneficios";
import Premios from "../assets/screens/tabBar/Premios";
import Cartao from "../assets/screens/tabBar/Cartao";

const size = 20;

const TabBar = createBottomTabNavigator(
  {
    Inicio: {
      screen: Inicio,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="home" color={tintColor} size={size} />
        ),
        title: "Início"
      }
    },
    Beneficios: {
      screen: Beneficios,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="user-friends" color={tintColor} size={size} />
        ),
        title: "Clube"
      }
    },
    Premios: {
      screen: Premios,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="medal" color={tintColor} size={size} />
        ),
        title: "Prêmios"
      }
    },
    Cartao: {
      screen: Cartao,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="qrcode" color={tintColor} size={size} />
        ),
        title: "Cartão"
      }
    }
  },
  {
    tabBarOptions: {
      activeTintColor: "#4d4d4d",
      inactiveTintColor: "#b3b3b3",
      activeBackgroundColor: "#e6e6e6",
      style: {
        backgroundColor: "#f2f2f2"
      },
      labelStyle: {
        marginTop: -10,
        marginBottom: 5
      }
    },
    initialRouteName: "Inicio",
    lazy: false
  }
);

export default TabBar;
