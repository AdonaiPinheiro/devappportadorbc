import React, { Component } from "react";
import { ScrollView, SafeAreaView, View, Dimensions } from "react-native";
import { createDrawerNavigator, DrawerItems } from "react-navigation";

//TabBar
import TabBar from "./tabBarRoutes";
import Header from "../assets/components/drawer/Header";
import Footer from "../assets/components/drawer/Footer";

//Icons
import Icon from "react-native-vector-icons/FontAwesome5";

//Styles
import styles from "../assets/styles/drawer";

//Screens
import Perfil from "../assets/screens/drawer/Perfil";
import Notificacoes from "../assets/screens/drawer/Notificacoes";
import Termos from "../assets/screens/drawer/Termos";
import Mensagens from "../assets/screens/drawer/Mensagens";
import Ajuda from "../assets/screens/drawer/Ajuda";

//CustomDrawer
const DrawerContent = props => (
  <SafeAreaView style={styles.comp}>
    <View style={styles.areaContent}>
      <Header />
      <ScrollView>
        <DrawerItems {...props} />
      </ScrollView>
      <Footer />
    </View>
  </SafeAreaView>
);

//Pegando as dimensões da tela para setar o edgeWidth
var { width } = Dimensions.get("window");

const Drawer = createDrawerNavigator(
  {
    //Importando a TabBar para o projeto inteiro
    TabBar: {
      screen: TabBar,
      navigationOptions: {
        drawerLabel: () => null
      }
    },
    //Importando as screens do Drawer
    Perfil: {
      screen: Perfil,
      navigationOptions: {
        drawerIcon: ({ tintColor }) => (
          <Icon name="user" size={18} light color={tintColor} />
        ),
        title: "Perfil"
      }
    },
    Ajuda: {
      screen: Ajuda,
      navigationOptions: {
        drawerIcon: ({ tintColor }) => (
          <Icon name="question-circle" size={18} color={tintColor} />
        ),
        title: "Ajuda"
      }
    },
    Notificacoes: {
      screen: Notificacoes,
      navigationOptions: {
        drawerIcon: ({ tintColor }) => (
          <Icon name="bell" size={18} color={tintColor} />
        ),
        title: "Notificações"
      }
    },
    Termos: {
      screen: Termos,
      navigationOptions: {
        drawerIcon: ({ tintColor }) => (
          <Icon name="file-alt" size={18} light color={tintColor} />
        ),
        title: "Termos de Uso"
      }
    },
    Mensagens: {
      screen: Mensagens,
      navigationOptions: {
        drawerIcon: ({ tintColor }) => (
          <Icon name="comment-dots" size={18} light color={tintColor} />
        ),
        title: "Mensagens"
      }
    }
  },
  {
    defaultNavigationOptions: {},
    edgeWidth: 50 - width,
    drawerWidth: width * 0.65,
    contentOptions: {
      activeTintColor: "#001F4D",
      inactiveTintColor: "#4A4A4A",
      itemStyle: {
        justifyContent: "center",
        marginLeft: -18
      },
      labelStyle: {
        marginLeft: -10
      }
    },
    initialRouteName: "TabBar",
    contentComponent: DrawerContent,
    drawerPosition: "right",
    overlayColor: "#4A4A4A",
    drawerType: "front"
  }
);

export default Drawer;
