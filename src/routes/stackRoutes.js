import React, { Component } from "react";
import { createAppContainer, createSwitchNavigator } from "react-navigation";

//Screens
import Preload from "../assets/screens/stack/preload";
import Login from "../assets/screens/stack/login";
import Drawer from "./drawerRoutes";

const StackNavigator = createSwitchNavigator(
  {
    Preload: {
      screen: Preload
    },
    Login: {
      screen: Login
    },
    Main: {
      screen: Drawer
    }
  },
  {
    initialRouteName: "Main",
    defaultNavigationOptions: {
      header: null
    }
  }
);

export default createAppContainer(StackNavigator);
