import React, { Component } from "react";
import "./configs/StatusBarConfig";

//Main APP
import Routes from "./routes/stackRoutes";

console.disableYellowBox = true;

const App = () => <Routes />;

export default App;
