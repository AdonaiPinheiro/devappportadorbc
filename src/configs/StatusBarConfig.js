import { StatusBar } from "react-native";

StatusBar.setBackgroundColor("#f2f2f2");
StatusBar.setBarStyle("dark-content");
StatusBar.setHidden(false);
