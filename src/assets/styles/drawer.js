import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  comp: {
    flex: 1,
    justifyContent: "space-between",
    alignItems: "stretch"
  },
  areaContent: {
    flex: 1,
    justifyContent: "center"
  },
  headerComp: {
    height: 140,
    backgroundColor: "#a6a6a6",
    alignItems: "center",
    justifyContent: "center",
    padding: 10
  },
  footerComp: {
    height: 40,
    flexDirection: "row",
    backgroundColor: "#FFFF",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 10
  },
  profile: {
    marginBottom: 5
  },
  profileImg: {
    width: 80,
    height: 80,
    borderRadius: 40,
    borderWidth: 2,
    borderColor: "#001f4d",
    backgroundColor: "#FFF"
  },
  profileText: {
    fontSize: 14,
    color: "#001f4d"
  },
  footerText: {
    fontSize: 14,
    color: "#FF0000"
  },
  logoutButton: {
    alignItems: "center",
    justifyContent: "center"
  }
});

export default styles;
