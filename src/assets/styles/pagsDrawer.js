import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  header: {
    flex: 0.12,
    alignItems: "center",
    justifyContent: "center"
  },
  body: {
    flex: 0.88,
    marginLeft: 10,
    marginRight: 10
  },
  textTitulo: {
    fontSize: 18,
    fontFamily: "Rui Abreu - AzoSans-Bold"
  },
  textBody: {
    fontSize: 18,
    textAlign: "justify",
    fontFamily: "Rui Abreu - AzoSans-Regular"
  },
  inputPerfil: {
    width: "100%",
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 0.3,
    borderColor: "#4A4A4A",
    borderRadius: 5,
    alignSelf: "center",
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    padding: 5,
    paddingLeft: 10
  },
  saveButton: {
    width: "100%",
    height: 60,
    borderWidth: 1,
    borderColor: "#CCC",
    borderRadius: 5,
    alignSelf: "center",
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    padding: 5,
    paddingLeft: 10,
    backgroundColor: "#CCC",
    alignItems: "center",
    justifyContent: "center"
  },
  textButtonSave: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#FFF"
  },
  //Ajuda Styles
  FAQtitle: {
    height: 40,
    justifyContent: "center",
    padding: 10,
    backgroundColor: "#003380",
    marginTop: -10,
    borderRadius: 5
  },
  FAQtitleText: {
    fontSize: 16,
    fontWeight: "bold",
    color: "#fff"
  },
  FAQcontent: {
    backgroundColor: "#F1F1F1",
    padding: 10,
    marginTop: 5,
    borderRadius: 5
  },
  FAQcontentText: {
    fontSize: 16
  },
  //Swiper
  areaSwiper: {
    flex: 1,
    width: "100%",
    padding: 10,
    minHeight: 330,
    alignSelf: "center"
  },
  //ScrollViewPremios
  scrollViewPremios: {
    flex: 1,
    marginTop: 10
  }
});

export default styles;
