import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10
  },
  header: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  menuButton: {
    width: 50,
    height: 50,
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    fontWeight: "bold",
    fontSize: 16,
    marginTop: -5
  },
  img: {
    height: 39,
    width: 223
  }
});

export default styles;
