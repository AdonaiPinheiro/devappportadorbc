import React, { Component } from "react";
import { TextInput, View, TouchableOpacity } from "react-native";

//Icons
import Icon from "react-native-vector-icons/FontAwesome5";

//Style
import styles from "./styles";

export default class Search extends Component {
  constructor(props) {
    super(props);

    this.state = {
      search: "",
      textAlt: ""
    };

    this.searchInput = this.searchInput.bind(this);
    this.search = this.search.bind(this);
  }

  searchInput(text) {
    let s = this.state;
    s.textAlt = text;
    this.setState(s);
  }

  search() {
    let s = this.state;
    s.search = s.textAlt;
    this.setState(s);
    alert(s.search);
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={{ flex: 1 }}
          placeholder="Buscar produto"
          onChangeText={text => {
            this.searchInput(text);
          }}
        />
        <TouchableOpacity
          onPress={() => {
            this.search();
          }}
        >
          <Icon name="search" size={24} color="#CCC" />
        </TouchableOpacity>
      </View>
    );
  }
}
