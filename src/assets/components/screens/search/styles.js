import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 40,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderColor: "#ccc",
    paddingRight: 5,
    paddingLeft: 5,
    borderWidth: 0.5,
    borderRadius: 5,
    marginBottom: 5
  }
});

export default styles;
