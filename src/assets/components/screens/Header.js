import React, { Component } from "react";
import { Text, View, TouchableOpacity, Image } from "react-native";
import { withNavigation } from "react-navigation";
import Accordion from "react-native-collapsible/Accordion";

//Styles
import styles from "../../styles/Header";

//Icons
import Icon from "react-native-vector-icons/FontAwesome5";

const img = require("../../images/transparente/main_logo.png");

export class Header extends Component {
  constructor(props) {
    super(props);

    this.state = { sizeMenu: 0 };
    if (this.props.menu == null) {
      this.state.sizeMenu = 24;
    } else {
      this.state.sizeMenu = this.props.menu;
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.goBack();
          }}
        >
          <View style={[styles.menuButton, { alignItems: "flex-start" }]}>
            <Icon
              name="chevron-left"
              size={this.props.size}
              solid
              color="#4A4A4A"
            />
          </View>
        </TouchableOpacity>

        <View style={styles.header}>
          <Image style={styles.img} source={img} />
        </View>

        <TouchableOpacity
          onPress={() => {
            this.props.navigation.openDrawer();
          }}
        >
          <View style={[styles.menuButton, { alignItems: "flex-end" }]}>
            <Icon
              name="bars"
              size={this.state.sizeMenu}
              solid
              color="#4A4A4A"
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default withNavigation(Header);
