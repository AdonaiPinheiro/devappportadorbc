import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  StyleSheet,
  BackHandler
} from "react-native";
import Modal from "react-native-modalbox";

//Icons
import Icon from "react-native-vector-icons/FontAwesome5";

export default class Dropdown extends Component {
  constructor(props) {
    super(props);

    this.state = {
      swipeToClose: true
    };

    this.abrirModal = this.abrirModal.bind(this);
  }

  abrirModal() {
    this.refs.modal1.open();
  }

  render() {
    return (
      <View
        style={{
          justifyContent: "center",
          alignItems: "center",
          marginTop: -20,
          marginBottom: 5
        }}
      >
        <TouchableOpacity
          style={{
            width: "50%",
            alignItems: "center",
            justifyContent: "center"
          }}
          onPress={this.abrirModal}
        >
          <Icon name="caret-down" size={24} />
        </TouchableOpacity>

        <Modal
          style={[styles.modal, styles.modal4]}
          position={"top"}
          ref={"modal1"}
          entry={"top"}
          coverScreen={true}
          animationDuration={600}
          backButtonClose={true}
        >
          <StatusBar barStyle="dark-content" backgroundColor="#CCC" />
          <Text style={styles.text}>Brazilian Card</Text>
          <View style={styles.barra} />
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  barra: {
    height: 3,
    width: 75,
    borderRadius: 6,
    backgroundColor: "#000",
    alignSelf: "center",
    margin: 10
  },
  modal: {
    justifyContent: "flex-end",
    alignItems: "center",
    borderBottomRightRadius: 15,
    borderBottomLeftRadius: 15
  },
  modal4: {
    height: 150
  },
  text: {
    color: "black",
    fontSize: 14,
    borderWidth: 0.5,
    padding: 3,
    borderRadius: 5
  }
});
