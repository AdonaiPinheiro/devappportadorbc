import React, { Component } from "react";
import { Text, ScrollView } from "react-native";

//Components
import Pontos from "./pontos";
import TransPont from "./transPont";
import LastTrasacao from "./lastTrans";

export default class PontosMain extends Component {
  render() {
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{
          flex: 1,
          width: "100%",
          height: "100%",
          paddingLeft: 2.5,
          paddingRight: 2.5
        }}
      >
        <Pontos />
        <TransPont />
        <LastTrasacao />
      </ScrollView>
    );
  }
}
