import React, { Component } from "react";
import { View, Text } from "react-native";

//Icon
import Icon from "react-native-vector-icons/FontAwesome5";

//Styles
import styles from "../styles";

export default class Acao extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tipo: this.props.tipo,
      data: this.props.data,
      local: this.props.local,
      preco: this.props.preco,
      cod: this.props.cod,
      pontos: this.props.pontos
    };
  }

  render() {
    let s = this.state;

    return (
      <View>
        <View style={styles.areaMidContent}>
          <Text style={styles.lastTitulo}>{s.tipo}</Text>
          <Text style={styles.lastData}>{s.data}</Text>
        </View>
        <View style={[styles.areaMidContent, { marginTop: 0 }]}>
          <Text style={{ fontSize: 14 }}>{s.local}</Text>
          <Text style={{ color: "#F5A623", fontWeight: "bold" }}>
            {s.pontos} pontos
          </Text>
        </View>
        <View style={[styles.areaMidContent, { marginTop: 0 }]}>
          <Text style={{ fontSize: 12 }}>CÓD. {s.cod}</Text>
          <Text style={styles.lastPreco}>R$ {s.preco}</Text>
        </View>
      </View>
    );
  }
}
