import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    width: "100%",
    backgroundColor: "#F5A623",
    borderRadius: 5,
    marginBottom: 10,
    padding: 10,
    paddingLeft: 36,
    paddingRight: 36,
    paddingTop: 10,
    paddingBottom: 10,
    justifyContent: "space-between"
  },
  areaSaldo: {
    borderWidth: 0.5,
    borderRadius: 5,
    borderColor: "#FFF",
    alignItems: "center",
    justifyContent: "center",
    width: "60%",
    minWidth: 60
  },
  textSaldo: {
    fontSize: 12,
    color: "#FFF"
  },
  text: {
    color: "#FFF",
    fontSize: 16,
    fontWeight: "bold"
  }
});

export default styles;
