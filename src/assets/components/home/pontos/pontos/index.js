import React, { Component } from "react";
import { Text, View } from "react-native";

//Icons
import Icon from "react-native-vector-icons/FontAwesome5";

//Styles
import styles from "./styles";

export default class Pontos extends Component {
  render() {
    return (
      <View style={[styles.container, { minHeight: 55 }]}>
        <View>
          <Icon name="medal" size={20} color="#FFF" light />
          <Text style={[styles.text, { fontSize: 12 }]}>PONTOS</Text>
        </View>
        <View style={{ flexDirection: "row" }}>
          <View style={{ marginLeft: 10, alignItems: "center" }}>
            <View style={styles.areaSaldo}>
              <Text style={styles.textSaldo}>Saldo</Text>
            </View>
            <Text style={styles.text}>6543</Text>
          </View>
          <View
            style={{
              height: "85%",
              width: 1,
              backgroundColor: "#FFF",
              marginLeft: 10,
              alignSelf: "center"
            }}
          />
          <View style={{ marginLeft: 10, alignItems: "center" }}>
            <View style={styles.areaSaldo}>
              <Text style={styles.textSaldo}>Total</Text>
            </View>
            <Text style={styles.text}>10543</Text>
          </View>
        </View>
      </View>
    );
  }
}
