import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";

//Styles
import styles from "./styles";

//Icon
import Icon from "react-native-vector-icons/FontAwesome5";

//Components
import Acao from "./acao";

export default class LastTrasacao extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showFat: false,
      icon: "angle-down"
    };
  }

  active = () => {
    let s = this.state;

    if (s.showFat) {
      s.icon = "angle-down";
      s.showFat = false;
    } else {
      s.icon = "angle-up";
      s.showFat = true;
    }

    this.setState(s);
  };

  showFatura = () => {
    let s = this.state;
    if (s.showFat) {
      return (
        <View style={styles.last}>
          <View style={styles.areaTitulo}>
            <Icon name="file-invoice-dollar" size={20} color="#1213" light />
            <Text style={styles.textTitulo}>TRANSAÇÕES DE DÉBITO</Text>
          </View>
          <Acao
            tipo="Pagamento de fatura"
            data="Hoje"
            local="POSTO DUQUE FILIAL"
            preco="500,00"
            cod="2101212310000"
          />
          <Acao
            tipo="Crédito"
            data="08/12/1995"
            local="POSTO DUQUE FILIAL"
            preco="1.000,00"
            cod="2101212310000"
          />
          <Acao
            tipo="Pagamento de fatura"
            data="Hoje"
            local="POSTO DUQUE FILIAL"
            preco="500,00"
            cod="2101212310000"
          />
        </View>
      );
    } else {
      return;
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          activeOpacity={0.5}
          style={{
            flex: 1,
            width: "100%",
            alignItems: "center",
            justifyContent: "center"
          }}
          onPress={this.active}
        >
          <Icon name={this.state.icon} size={24} color="#001f4d" solid />
        </TouchableOpacity>
        {this.showFatura()}
      </View>
    );
  }
}
