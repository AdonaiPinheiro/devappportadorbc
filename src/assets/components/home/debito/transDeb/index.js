import React, { Component } from "react";
import { Text, View } from "react-native";

//Icons
import Icon from "react-native-vector-icons/FontAwesome5";

//Styles
import styles from "./styles";

export default class TransDeb extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.areaTitulo}>
          <Icon name="donate" size={20} color="#1213" light />
          <Text style={styles.textTitulo}>TRANSAÇÕES DE DÉBITO</Text>
        </View>
        <View style={styles.areaMidContent}>
          <Text style={styles.textMoeda}>R$</Text>
          <Text style={styles.textPreco}>2.345,67</Text>
        </View>
        <Text style={styles.textRodape}>
          ÚLTIMA TRANSAÇÃO 17/01 Pagamento de fatura
        </Text>
      </View>
    );
  }
}
