import React, { Component } from "react";
import { Text, ScrollView } from "react-native";

//Components
import Debito from "./debito";
import TransDeb from "./transDeb";
import LastTrasacao from "./lastTrans";

export default class DebitoMain extends Component {
  render() {
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{
          flex: 1,
          width: "100%",
          height: "100%",
          paddingLeft: 2.5,
          paddingRight: 2.5
        }}
      >
        <Debito />
        <TransDeb />
        <LastTrasacao />
      </ScrollView>
    );
  }
}
