import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";

//Styles
import styles from "./styles";

//Icon
import Icon from "react-native-vector-icons/FontAwesome5";

//Components
import Acao from "./acao";

export default class LastTrasacao extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showFat: false,
      icon: "angle-down"
    };
  }

  active = () => {
    let s = this.state;

    if (s.showFat) {
      s.icon = "angle-down";
      s.showFat = false;
    } else {
      s.icon = "angle-up";
      s.showFat = true;
    }

    this.setState(s);
  };

  showFatura = () => {
    let s = this.state;
    if (s.showFat) {
      return (
        <View style={styles.last}>
          <View style={styles.areaTitulo}>
            <Icon name="file-invoice-dollar" size={20} color="#1213" light />
            <Text style={styles.textTitulo}>
              TRANSAÇÕES DE CLUBE DO CARIMBO
            </Text>
          </View>
          <Acao
            tipo="Cancelamento de venda"
            data="Hoje"
            local="POSTO DUQUE FILIAL"
            cod="2101212310000"
            carimbos="500"
          />
          <Acao
            tipo="Compra a vista"
            data="08/12/1995"
            local="POSTO DUQUE FILIAL"
            cod="2101212310000"
            carimbos="40"
          />
          <Acao
            tipo="Resgate de produto"
            data="Hoje"
            local="POSTO DUQUE FILIAL"
            cod="2101212310000"
            carimbos="3"
          />
        </View>
      );
    } else {
      return;
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          activeOpacity={0.5}
          style={{
            flex: 1,
            width: "100%",
            alignItems: "center",
            justifyContent: "center"
          }}
          onPress={this.active}
        >
          <Icon name={this.state.icon} size={24} color="#001f4d" solid />
        </TouchableOpacity>
        {this.showFatura()}
      </View>
    );
  }
}
