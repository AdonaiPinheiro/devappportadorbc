import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 5,
    marginTop: -5
  },
  last: {
    flex: 1,
    width: "100%",
    borderColor: "#CCC",
    borderWidth: 0.5,
    borderRadius: 5,
    marginVertical: 5,
    padding: 10
  },
  areaTitulo: {
    flexDirection: "row",
    alignItems: "center"
  },
  textTitulo: {
    marginLeft: 10,
    color: "#9013FE",
    fontSize: 12,
    fontWeight: "bold"
  },
  areaMidContent: {
    flexDirection: "row",
    marginTop: 10,
    alignItems: "flex-start",
    justifyContent: "space-between"
  },
  lastTitulo: {
    fontSize: 14,
    fontWeight: "bold"
  },
  lastData: {
    fontSize: 12,
    fontWeight: "100"
  },
  lastPreco: {
    color: "#50E3C2",
    fontWeight: "bold"
  }
});

export default styles;
