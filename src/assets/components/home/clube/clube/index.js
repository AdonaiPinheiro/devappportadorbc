import React, { Component } from "react";
import { Text, View } from "react-native";

//Icons
import Icon from "react-native-vector-icons/FontAwesome5";

//Styles
import styles from "./styles";

export default class Clube extends Component {
  render() {
    return (
      <View style={[styles.container, { minHeight: 55 }]}>
        <View>
          <Icon name="stamp" size={20} color="#FFF" light />
          <Text style={[styles.text, { fontSize: 12 }]}>CLUBE DO CARIMBO</Text>
        </View>
        <View style={{ flexDirection: "row" }}>
          <View style={{ marginLeft: 10, alignItems: "center" }}>
            <View style={styles.areaSaldo}>
              <Text style={styles.textSaldo}>Total</Text>
            </View>
            <Text style={styles.text}>543</Text>
          </View>
        </View>
      </View>
    );
  }
}
