import React, { Component } from "react";
import { Text, ScrollView } from "react-native";

//Components
import Clube from "./clube";
import TransClube from "./transClube";
import LastTrasacao from "./lastTrans";

export default class ClubeMain extends Component {
  render() {
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{
          flex: 1,
          width: "100%",
          height: "100%",
          paddingLeft: 2.5,
          paddingRight: 2.5
        }}
      >
        <Clube />
        <TransClube />
        <LastTrasacao />
      </ScrollView>
    );
  }
}
