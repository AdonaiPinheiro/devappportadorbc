import React, { Component } from "react";
import { Text, ScrollView } from "react-native";

//Components
import Credito from "./credito";
import FatAtual from "./fatAtual";
import FatFec from "./fetFechada";
import LastTrasacao from "./lastTrans";

export default class CreditoMain extends Component {
  render() {
    return (
      <ScrollView
        showsVerticalScrollIndicator={true}
        style={{
          flex: 1,
          width: "100%",
          height: "100%",
          paddingLeft: 2.5,
          paddingRight: 2.5
        }}
        scrollEnabled={true}
      >
        <Credito />
        <FatAtual />
        <LastTrasacao />
        <FatFec />
      </ScrollView>
    );
  }
}
