import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    width: "100%",
    backgroundColor: "#fff",
    borderRadius: 5,
    marginBottom: 10,
    borderWidth: 0.5,
    padding: 10,
    borderColor: "#00aaff"
  },
  areaTitulo: {
    flexDirection: "row",
    alignItems: "center"
  },
  areaMidContent: {
    flexDirection: "row",
    alignItems: "flex-start"
  },
  textTitulo: {
    marginLeft: 10,
    color: "#00aaff",
    fontSize: 12,
    fontWeight: "bold"
  },
  textMoeda: {
    marginTop: 10,
    color: "#00aaff",
    fontSize: 14,
    fontWeight: "bold"
  },
  textPreco: {
    marginLeft: 10,
    color: "#00aaff",
    fontSize: 36,
    fontWeight: "bold"
  },
  textRodape: {
    fontSize: 12,
    color: "#1215",
    alignSelf: "center"
  }
});

export default styles;
