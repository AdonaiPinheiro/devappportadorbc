import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    width: "100%",
    backgroundColor: "#fff",
    borderRadius: 5,
    marginBottom: 10,
    borderWidth: 0.5,
    padding: 10,
    borderColor: "#ff4d4d"
  },
  areaTitulo: {
    flexDirection: "row",
    alignItems: "center"
  },
  textTitulo: {
    marginLeft: 10,
    color: "#ff4d4d",
    fontSize: 12,
    fontWeight: "bold"
  },
  areaMidContent: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  areaBoleto: {
    borderRadius: 5,
    borderWidth: 2,
    borderColor: "#ff4d4d",
    paddingLeft: 10,
    paddingRight: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  textMoeda: {
    marginTop: 10,
    color: "#ff4d4d",
    fontSize: 14,
    fontWeight: "bold"
  },
  textPreco: {
    marginLeft: 10,
    color: "#ff4d4d",
    fontSize: 36,
    fontWeight: "bold"
  },
  textRodape: {
    fontSize: 12,
    color: "#ff4d4d",
    alignSelf: "center"
  }
});

export default styles;
