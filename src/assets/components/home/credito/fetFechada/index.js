import React, { Component } from "react";
import { Text, View } from "react-native";

//Icons
import Icon from "react-native-vector-icons/FontAwesome5";

//Styles
import styles from "./styles";

export default class FatFec extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.areaTitulo}>
          <Icon name="receipt" size={20} color="#1213" light />
          <Text style={styles.textTitulo}>FATURAL FECHADA</Text>
        </View>
        <View style={styles.areaMidContent}>
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.textMoeda}>R$</Text>
            <Text style={styles.textPreco}>2.345,67</Text>
          </View>
          <View style={styles.areaBoleto}>
            <Icon name="barcode" size={24} color="#121" />
            <Text style={{ fontSize: 10 }}>Gerar Boleto</Text>
          </View>
        </View>
        <Text style={styles.textRodape}>VENCIMENTO 10 JAN</Text>
      </View>
    );
  }
}
