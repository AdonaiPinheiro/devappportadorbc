import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 150,
    backgroundColor: "transparent",
    borderRadius: 5,
    marginBottom: 10,
    padding: 10,
    paddingTop: 45,
    justifyContent: "center"
  },
  areaTitulo: {
    flexDirection: "row",
    alignItems: "center"
  },
  areaSwiper: {
    flex: 1,
    width: "100%",
    height: "100%",
    alignSelf: "center"
  },
  textTitulo: {
    marginLeft: 10,
    color: "#00aaff",
    fontSize: 12,
    fontWeight: "bold"
  }
});

export default styles;
