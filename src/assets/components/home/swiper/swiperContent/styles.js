import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    height: "100%",
    backgroundColor: "transparent"
  },
  areaContent: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start"
  },
  textTitulo: {
    color: "#00aaff",
    fontSize: 15,
    marginTop: 5
  },
  textDesc: {
    color: "#121",
    fontSize: 13
  },
  textData: {
    color: "#ff4d4d",
    fontSize: 12
  },
  img: {
    height: 75,
    width: 60,
    backgroundColor: "#CCC",
    marginRight: 10,
    borderRadius: 5
  },
  imgModal: {
    height: 150,
    width: 120,
    borderRadius: 10,
    backgroundColor: "#CCC"
  },
  modalContent: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
});

export default styles;
