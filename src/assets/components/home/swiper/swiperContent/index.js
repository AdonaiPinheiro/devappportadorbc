import React, { Component } from "react";
import { Text, View, Image, TouchableOpacity, StatusBar } from "react-native";

//Styles
import styles from "./styles";

//Modal
import Modal from "react-native-modalbox";

export default class SwiperContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      titulo: this.props.titulo,
      descricao: "Confira as regras do sorteio",
      data: this.props.data,
      regras: this.props.regras,
      img: this.props.img
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.areaContent}
          activeOpacity={0.7}
          onPress={() => {
            this.refs.premio.open();
          }}
        >
          <View>
            <Text style={styles.textTitulo}>{this.state.titulo}</Text>
            <Text style={styles.textDesc}>{this.state.descricao}</Text>
            <Text style={styles.textData}>{this.state.data}</Text>
          </View>
          <View>
            <Image source={{ uri: this.state.img }} style={styles.img} />
          </View>
        </TouchableOpacity>

        <Modal
          position={"bottom"}
          ref={"premio"}
          entry={"bottom"}
          coverScreen={true}
          animationDuration={400}
          backdropColor={"#CCC"}
          backButtonClose={true}
        >
          <StatusBar barStyle="dark-content" backgroundColor="#CCC" />
          <View style={styles.modalContent}>
            <Image source={{ uri: this.state.img }} style={styles.imgModal} />
            <Text style={{ fontWeight: "bold", margin: 10, fontSize: 16 }}>
              {this.state.titulo}
            </Text>
            <Text style={{ textAlign: "left" }}>1º Prêmio: </Text>
            <Text style={{ textAlign: "left" }}>2º Prêmio: </Text>
            <Text style={{ textAlign: "left" }}>3º Prêmio: </Text>
            <Text style={{ textAlign: "left" }}>4º Prêmio: </Text>
            <Text style={{ textAlign: "left" }}>5º Prêmio: </Text>
            <Text
              style={{
                fontWeight: "bold",
                marginTop: 10,
                padding: 15,
                textAlign: "justify"
              }}
            >
              Regras do Sorteio:{this.state.regras}
            </Text>
          </View>
        </Modal>
      </View>
    );
  }
}
