import React, { Component } from "react";
import { Text, View } from "react-native";

//Components
import SwiperContent from "./swiperContent";

//Icons
import Icon from "react-native-vector-icons/FontAwesome5";

//Styles
import styles from "./styles";

//Swiper
import Swiper from "react-native-swiper";

export default class HomeSwiper extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.areaTitulo}>
          <Icon name="ticket-alt" size={20} color="#1213" light />
          <Text style={styles.textTitulo}>SORTEIOS</Text>
        </View>
        <Swiper
          style={styles.areaSwiper}
          autoplay
          autoplayTimeout={5}
          showsButtons={false}
          showsPagination={false}
          paginationStyle={{
            bottom: 10,
            center: 10
          }}
          loop
        >
          <SwiperContent
            titulo="FIFA 19 DELUXE EDITION"
            img="https://i11d.3djuegos.com/juegos/14368/fifa_18/fotos/ficha/fifa_18-3836575.jpg"
            data="SORTEIO 30 MAR"
            regras="Jogar sem o Neymar. Jogar sem o Neymar. Jogar sem o Neymar. Jogar sem o Neymar. Jogar sem o Neymar. Jogar sem o"
          />
          <SwiperContent
            titulo="GOD OF WAR 4"
            img="https://images-na.ssl-images-amazon.com/images/I/913r59lGp-L._SX342_.jpg"
            data="SORTEIO 29 MAR"
            regras="Garoto venha!"
          />
          <SwiperContent
            titulo="GTA V"
            img="http://3.bp.blogspot.com/-H_wzeAeUOAc/VSmrSaEdQjI/AAAAAAAABW4/22c-DytpuE4/s1600/Capa_GTAV_PC.jpg"
            data="SORTEIO 29 MAR"
            regras="Não matar pedestres!"
          />
        </Swiper>
      </View>
    );
  }
}
