import React, { Component } from "react";
import { Text, View, Image } from "react-native";
import { withNavigation } from "react-navigation";

//Style
import styles from "../../styles/drawer";

//Gradient Component
import LinearGradient from "react-native-linear-gradient";

const imgProfile = {
  uri:
    "https://media.licdn.com/dms/image/C4D03AQFRFVoQczXYjQ/profile-displayphoto-shrink_200_200/0?e=1563408000&v=beta&t=qdgoicpd-6X_HyW_GQexu15qbGoesFZZHptQ6g8hrtU"
};

export class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nome: "Adonai"
    };
  }

  render() {
    return (
      <LinearGradient colors={["#cccccc", "#fff"]} style={styles.headerComp}>
        <View style={styles.profile}>
          <Image source={imgProfile} style={styles.profileImg} />
        </View>
        <View>
          <Text style={styles.profileText}>Bem-vindo, {this.state.nome}</Text>
        </View>
        <View
          style={{
            width: 50,
            height: 2,
            borderRadius: 2,
            backgroundColor: "#001d4f",
            marginTop: 5
          }}
        />
      </LinearGradient>
    );
  }
}

export default withNavigation(Header);
