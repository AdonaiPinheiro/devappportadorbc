import React, { Component } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import { withNavigation } from "react-navigation";

//Icons
import Icon from "react-native-vector-icons/FontAwesome5";

//Style
import styles from "../../styles/drawer";

export class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {};

    this.logout = this.logout.bind(this);
  }

  logout() {}

  render() {
    return (
      <View style={styles.footerComp}>
        <View>
          <Text style={[styles.footerText, { fontSize: 10, color: "#4d4d4d" }]}>
            Brazilian Card v1.15.35
          </Text>
        </View>
        <TouchableOpacity style={styles.logoutButton}>
          <View onPress={this.logout}>
            <Text style={[styles.footerText, { fontWeight: "bold" }]}>
              Sair
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default withNavigation(Header);
