import React, { Component } from "react";
import { Text, View, Image } from "react-native";
import QRCode from "react-native-qrcode";

//Styles
import styles from "./styles";

const img = require("../../images/transparente/logo_corInvertida.png");

export default class CartaoForm extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{ overflow: "hidden", marginTop: 20 }}>
          <QRCode
            value={this.props.qrcode}
            size={125}
            bgColor="#FFF"
            fgColor="#001F4D"
          />
        </View>
        <Text style={[styles.text, { fontSize: 18 }]}>{this.props.nome}</Text>
        <Text style={[styles.text, { fontSize: 20 }]}>
          {this.props.numCartao}
        </Text>
        <View style={{ alignSelf: "flex-end" }}>
          <Text style={[styles.text, styles.textVal]}>Validade</Text>
          <Text style={[styles.text, styles.textVal]}>{this.props.val}</Text>
        </View>
        <Image style={styles.img} source={img} />
      </View>
    );
  }
}
