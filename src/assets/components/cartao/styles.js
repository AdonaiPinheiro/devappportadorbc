import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "95%",
    height: "100%",
    backgroundColor: "#001F4D",
    borderRadius: 10,
    justifyContent: "space-between",
    alignItems: "center",
    padding: 20,
    marginBottom: 10
  },
  img: {
    width: 200,
    height: 35
  },
  text: {
    color: "#FFF",
    fontFamily: "Rui Abreu - AzoSans-Regular"
  },
  textVal: {
    fontSize: 14,
    alignSelf: "flex-end",
    marginRight: 20,
    fontStyle: "italic",
    fontFamily: "Rui Abreu - AzoSans-Regular"
  }
});

export default styles;
