import React, { Component } from "react";
import { Text, View } from "react-native";

//Styles
import styles from "./styles";

//Icon
import Icon from "react-native-vector-icons/FontAwesome5";

export default class Beneficio extends Component {
  constructor(props) {
    super(props);

    this.state = {
      listServices: []
    };
  }

  componentWillMount() {
    this.state.listServices = this.props.data.servicos;
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.contentArea}>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <View>
              <Text style={styles.textTitle}>
                {this.state.listServices[1].nome}
              </Text>
              <Text style={[styles.textLocal, { textTransform: "uppercase" }]}>
                {this.props.data.item}
              </Text>
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={[styles.textLocal, { marginRight: 5, fontSize: 12 }]}
                >
                  Callback: {this.state.listServices[0].callBack}
                </Text>
                <Text style={[styles.textLocal, { fontSize: 12 }]}>
                  Desconto: {this.state.listServices[0].desconto}
                </Text>
              </View>
            </View>
          </View>
          <View
            style={{ justifyContent: "flex-start", alignItems: "flex-end" }}
          >
            <Text
              style={[styles.textTitle, { textAlign: "right", fontSize: 14 }]}
            >
              {this.state.listServices[0].data}
            </Text>
            <Text
              style={[
                styles.textLocal,
                { textAlign: "right", fontSize: 14, color: "#4A90E2" }
              ]}
            >
              R$ {this.state.listServices[0].preco}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}
