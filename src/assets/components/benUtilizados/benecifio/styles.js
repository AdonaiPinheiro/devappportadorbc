import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 70,
    alignItems: "center",
    justifyContent: "center"
  },
  contentArea: {
    width: "100%",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 10
  },
  textTitle: {
    fontWeight: "bold"
  },
  textLocal: {}
});

export default styles;
