import React, { Component } from "react";
import { FlatList } from "react-native";

//Premios
import Beneficio from "../benecifio";

export default class ListaBeneficios extends Component {
  constructor(props) {
    super(props);

    this.state = {
      listaBeneficios: [
        {
          id: "1",
          item: "Borracharia do Zé",
          local: "CAPOEIRAS - Florianópolis",
          end:
            "CEP 88070150 RUA PROFESSOR CLEMENTINO DE BRITO, 123 CAPOEIRAS - FLORIANÓPOLIS/SC",
          servicos: [
            {
              nome: "Balanceamento",
              cat: "Automotivos",
              callBack: "5%",
              desconto: "3%",
              preco: 500,
              data: "23/01/2017"
            },
            {
              nome: "Troca de Pneus",
              cat: "Automotivos",
              callBack: "5%",
              desconto: "3%",
              preco: 500,
              data: "23/01/2017"
            }
          ]
        },
        {
          id: "2",
          item: "Posto Duque Filial",
          local: "CAPOEIRAS - Florianópolis",
          end:
            "CEP 88070150 RUA PROFESSOR CLEMENTINO DE BRITO, 123 CAPOEIRAS - FLORIANÓPOLIS/SC",
          servicos: [
            {
              nome: "Balanceamento",
              cat: "Automotivos",
              callBack: "5%",
              desconto: "3%",
              preco: 500,
              data: "23/01/2017"
            },
            {
              nome: "Troca de Pneus",
              cat: "Automotivos",
              callBack: "5%",
              desconto: "3%",
              preco: 500,
              data: "23/01/2017"
            }
          ]
        },
        {
          id: "3",
          item: "Clínica Estética Sorriso",
          local: "CAPOEIRAS - Florianópolis",
          end:
            "CEP 88070150 RUA PROFESSOR CLEMENTINO DE BRITO, 123 CAPOEIRAS - FLORIANÓPOLIS/SC",
          servicos: [
            {
              nome: "Balanceamento",
              cat: "Automotivos",
              callBack: "5%",
              desconto: "3%",
              preco: 500,
              data: "23/01/2017"
            },
            {
              nome: "Troca de Pneus",
              cat: "Automotivos",
              callBack: "5%",
              desconto: "3%",
              preco: 500,
              data: "23/01/2017"
            }
          ]
        }
      ]
    };
  }

  render() {
    return (
      <FlatList
        data={this.state.listaBeneficios}
        renderItem={({ item }) => <Beneficio data={item} />}
        numColumns={1}
        horizontal={false}
        keyExtractor={(item, index) => item.id}
      />
    );
  }
}
