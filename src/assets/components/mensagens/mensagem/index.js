import React, { Component } from "react";
import { Text, Alert, TouchableOpacity } from "react-native";

//Styles
import styles from "./styles";

export default class Mensagem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      styleMsg: {},
      styleNome: { fontWeight: "bold" },
      nomeDisplay: ""
    };
  }

  componentWillMount() {
    let s = this.state;
    if (this.props.data.recebimento) {
      s.styleMsg = {
        backgroundColor: "#c2c2a3",
        borderColor: "#CCC",
        alignSelf: "flex-start"
      };
      s.nomeDisplay = this.props.data.nomeRecebimento;
    } else {
      s.styleMsg = {
        backgroundColor: "#66ff66",
        borderColor: "#33ff33",
        alignSelf: "flex-end"
      };
      s.nomeDisplay = this.props.data.nomeEnvio;
      s.styleNome = { ...s.styleNome, alignSelf: "flex-end" };
    }
    this.setState(s);
  }

  alertRemove = () => {
    Alert.alert(
      "Remover mensagem",
      "Deseja apagar está mensagem?",
      [
        {
          text: "Cancelar",
          onPress: () => console.log("Cancelado"),
          style: "cancel"
        },
        { text: "Sim", onPress: () => console.log("Apagar mensagem") }
      ],
      { cancelable: false }
    );
  };

  render() {
    let s = this.state;
    return (
      <TouchableOpacity
        onLongPress={this.alertRemove}
        style={[styles.container, s.styleMsg]}
      >
        <Text style={s.styleNome}>{s.nomeDisplay}</Text>
        <Text style={styles.msgArea}>{this.props.data.msg}</Text>
        <Text style={styles.msgHora}>{this.props.data.hora}</Text>
      </TouchableOpacity>
    );
  }
}
