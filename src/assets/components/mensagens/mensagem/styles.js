import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    borderRadius: 10,
    maxWidth: 200,
    padding: 5,
    marginVertical: 5,
    borderWidth: 0.5
  },
  msgArea: {
    fontSize: 14,
    textAlign: "justify"
  },
  msgHora: {
    fontSize: 12,
    alignSelf: "flex-end"
  }
});

export default styles;
