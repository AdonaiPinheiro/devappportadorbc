import React, { Component } from "react";
import { Text, View, TextInput } from "react-native";

//Styles
import styles from "./styles";

//Icon
import Icon from "react-native-vector-icons/FontAwesome5";

export default class Writer extends Component {
  render() {
    return (
      <View style={styles.container}>
        <TextInput
          numberOfLines={1}
          multiline={true}
          style={styles.textArea}
          placeholder="Digite sua mensagem"
        />
        <View style={styles.buttonSend}>
          <Icon name="chevron-right" size={24} color="#FFF" />
        </View>
      </View>
    );
  }
}
