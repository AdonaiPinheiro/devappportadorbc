import { StyleSheet, Dimensions } from "react-native";

const { width } = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    marginVertical: 5,
    marginTop: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  textArea: {
    width: width - 70,
    borderWidth: 0.5,
    borderColor: "#CCC",
    borderRadius: 10,
    paddingVertical: 5,
    marginRight: 10,
    paddingHorizontal: 10
  },
  buttonSend: {
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: "#001f4d",
    alignItems: "center",
    justifyContent: "center"
  }
});

export default styles;
