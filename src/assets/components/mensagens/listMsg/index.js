import React, { Component } from "react";
import { Text, View, FlatList } from "react-native";

//Components
import Mensagem from "../mensagem";

export default class ListaMensagens extends Component {
  constructor(props) {
    super(props);

    this.state = {
      listaMensagens: [
        {
          id: "1",
          data: "06/06/2019",
          hora: "11:59",
          recebimento: false,
          nomeEnvio: "Adonai",
          nomeRecebimento: "João",
          msg: "Fala meu brother"
        },
        {
          id: "2",
          data: "06/06/2019",
          hora: "12:00",
          recebimento: true,
          nomeEnvio: "Adonai",
          nomeRecebimento: "João",
          msg: "Fala meu brother"
        },
        {
          id: "3",
          data: "06/06/2019",
          hora: "12:01",
          recebimento: true,
          nomeEnvio: "Adonai",
          nomeRecebimento: "João",
          msg: "Fala meu brother"
        },
        {
          id: "4",
          data: "06/06/2019",
          hora: "15:59",
          recebimento: false,
          nomeEnvio: "Adonai",
          nomeRecebimento: "João",
          msg:
            "Fala meu brotherFala meu brotherFala meu brotherFala meu brother"
        },
        {
          id: "5",
          data: "06/06/2019",
          hora: "11:59",
          recebimento: false,
          nomeEnvio: "Adonai",
          nomeRecebimento: "João",
          msg: "Fala meu brother"
        },
        {
          id: "6",
          data: "06/06/2019",
          hora: "12:00",
          recebimento: true,
          nomeEnvio: "Adonai",
          nomeRecebimento: "João",
          msg: "Fala meu brother"
        },
        {
          id: "7",
          data: "06/06/2019",
          hora: "12:01",
          recebimento: true,
          nomeEnvio: "Adonai",
          nomeRecebimento: "João",
          msg: "Fala meu brother"
        },
        {
          id: "8",
          data: "06/06/2019",
          hora: "15:59",
          recebimento: false,
          nomeEnvio: "Adonai",
          nomeRecebimento: "João",
          msg:
            "Fala meu brotherFala meu brotherFala meu brotherFala meu brotherFala meu brotherFala meu brotherFala meu brotherFala meu brother"
        }
      ]
    };
  }

  render() {
    return (
      <FlatList
        data={this.state.listaMensagens}
        renderItem={({ item }) => <Mensagem data={item} />}
        numColumns={1}
        horizontal={false}
        inverted={true}
        keyExtractor={(item, index) => item.id}
      />
    );
  }
}
