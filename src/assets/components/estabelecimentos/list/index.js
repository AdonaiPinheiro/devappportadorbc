import React, { Component } from "react";
import { FlatList } from "react-native";

//Premios
import Estabelecimento from "../estabelecimento";

export default class ListaEstabelecimentos extends Component {
  constructor(props) {
    super(props);

    this.state = {
      listaEstabelecimentos: [
        {
          id: "1",
          item: "Borracharia do Zé",
          local: "CAPOEIRAS - Florianópolis",
          end:
            "CEP 88070150 RUA PROFESSOR CLEMENTINO DE BRITO, 123 CAPOEIRAS - FLORIANÓPOLIS/SC",
          servicos: [
            {
              nome: "Balanceamento",
              cat: "Automotivos",
              callBack: "5%",
              desconto: "3%"
            },
            {
              nome: "Troca de Pneus",
              cat: "Automotivos",
              callBack: "5%",
              desconto: "3%"
            }
          ]
        },
        {
          id: "2",
          item: "Posto Duque Filial",
          local: "CAPOEIRAS - Florianópolis",
          end:
            "CEP 88070150 RUA PROFESSOR CLEMENTINO DE BRITO, 123 CAPOEIRAS - FLORIANÓPOLIS/SC",
          servicos: [
            {
              nome: "Balanceamento",
              cat: "Automotivos",
              callBack: "5%",
              desconto: "3%"
            },
            {
              nome: "Troca de Pneus",
              cat: "Automotivos",
              callBack: "5%",
              desconto: "3%"
            }
          ]
        },
        {
          id: "3",
          item: "Clínica Estética Sorriso",
          local: "CAPOEIRAS - Florianópolis",
          end:
            "CEP 88070150 RUA PROFESSOR CLEMENTINO DE BRITO, 123 CAPOEIRAS - FLORIANÓPOLIS/SC",
          servicos: [
            {
              nome: "Balanceamento",
              cat: "Automotivos",
              callBack: "5%",
              desconto: "3%"
            },
            {
              nome: "Troca de Pneus",
              cat: "Automotivos",
              callBack: "5%",
              desconto: "3%"
            }
          ]
        }
      ]
    };
  }

  render() {
    return (
      <FlatList
        data={this.state.listaEstabelecimentos}
        renderItem={({ item }) => <Estabelecimento data={item} />}
        numColumns={1}
        horizontal={false}
        keyExtractor={(item, index) => item.id}
      />
    );
  }
}
