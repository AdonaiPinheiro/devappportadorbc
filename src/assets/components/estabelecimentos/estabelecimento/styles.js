import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#4A90E2",
    height: 70,
    marginVertical: 5,
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  contentArea: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 20
  },
  textTitle: {
    color: "#FFF",
    fontWeight: "bold"
  },
  textLocal: {
    color: "#FFF"
  },
  modal: {
    flex: 1,
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    padding: 20
  }
});

export default styles;
