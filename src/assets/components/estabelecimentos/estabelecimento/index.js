import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  TouchableOpacity,
  Image,
  FlatList
} from "react-native";

//Modal
import Modal from "react-native-modalbox";

//Styles
import styles from "./styles";

//Icon
import Icon from "react-native-vector-icons/FontAwesome5";

export default class Estabelecimento extends Component {
  constructor(props) {
    super(props);

    this.state = {
      listServices: []
    };
  }

  componentWillMount() {
    this.state.listServices = this.props.data.servicos;
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={() => {
            this.refs.estabelecimento.open();
          }}
          style={styles.contentArea}
        >
          <View>
            <Text style={styles.textTitle}>{this.props.data.item}</Text>
            <Text style={styles.textLocal}>{this.props.data.local}</Text>
          </View>
          <View>
            <Icon name="chevron-right" size={20} color="#FFF" solid />
          </View>
        </TouchableOpacity>

        <Modal
          style={styles.modal}
          position={"bottom"}
          ref={"estabelecimento"}
          entry={"bottom"}
          coverScreen={true}
          animationDuration={600}
          backdropColor={"#CCC"}
          backButtonClose={true}
        >
          <StatusBar barStyle="dark-content" backgroundColor="#CCC" />
          <View style={{ flex: 1, width: "100%" }}>
            <View style={{ alignItems: "center", justifyContent: "center" }}>
              <Text>Arraste ou clique para fechar</Text>
            </View>
            <View style={{ marginTop: 10 }}>
              <Text style={{ fontWeight: "bold", fontSize: 16 }}>
                {this.props.data.item}
              </Text>
            </View>
            <View style={{ marginTop: 10 }}>
              <Text style={{ color: "#4A90E2" }}>Endereço</Text>
              <Text>{this.props.data.end}</Text>
            </View>
            <View
              style={{
                marginTop: 10,
                height: 160,
                width: "100%",
                backgroundColor: "#CCC",
                borderRadius: 5
              }}
            >
              <Image
                style={{ height: 160, width: "100%" }}
                source={require("../../../images/mapa.png")}
              />
            </View>
            <View style={{ flexDirection: "row", marginTop: 20 }}>
              <Icon name="medal" size={24} solid color="#4A90E2" />
              <Text style={{ color: "#4A90E2", fontSize: 16, marginLeft: 5 }}>
                Serviços
              </Text>
            </View>
            <View style={{ marginTop: 10 }}>
              <FlatList
                data={this.state.listServices}
                renderItem={({ item }) => {
                  return (
                    <View style={[styles.container, { height: 80 }]}>
                      <View
                        style={[
                          styles.contentArea,
                          {
                            flexDirection: "column",
                            justifyContent: "center",
                            alignItems: "flex-start"
                          }
                        ]}
                      >
                        <Text style={styles.textTitle}>{item.nome}</Text>
                        <Text style={styles.textLocal}>{item.cat}</Text>
                        <View style={{ flexDirection: "row" }}>
                          <Text style={[styles.textLocal, { marginRight: 5 }]}>
                            Callback: {item.callBack}
                          </Text>
                          <Text style={styles.textLocal}>
                            Desconto: {item.desconto}
                          </Text>
                        </View>
                      </View>
                    </View>
                  );
                }}
                numColumns={1}
                horizontal={false}
                keyExtractor={(item, index) => item.id}
              />
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}
