import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    width: "100%",
    borderColor: "#F5A623",
    borderWidth: 0.5,
    borderRadius: 5,
    padding: 10,
    marginTop: 5,
    marginBottom: 5
  },
  textTitulo: {
    marginLeft: 10,
    color: "#F5A623",
    fontSize: 12,
    fontWeight: "bold"
  },
  textDone: {
    fontSize: 10,
    color: "#50E3C2",
    borderColor: "#50E3C2",
    borderWidth: 0.5,
    borderRadius: 5,
    paddingLeft: 5,
    paddingRight: 5
  }
});

export default styles;
