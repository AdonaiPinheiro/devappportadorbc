import React, { Component } from "react";
import { Text, View } from "react-native";

//Styles
import styles from "./styles";

//Icons
import Icon from "react-native-vector-icons/FontAwesome5";

export default class Resgates extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={{ flexDirection: "row" }}>
          <Icon name="shopping-cart" size={20} light />
          <Text style={styles.textTitulo}>ÚLTIMOS RESGATES</Text>
        </View>
        <View style={{ marginTop: 10, width: "100%" }}>
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <View>
              <Text style={{ fontWeight: "bold" }}>Resgate de produto</Text>
              <Text>CAMISA SUPER HERÓIS</Text>
              <Text style={{ fontSize: 12 }}>CÓD. RESGATE 2113201210000</Text>
            </View>
            <View style={{ alignItems: "flex-end" }}>
              <Text style={{ fontSize: 12 }}>10/01/2019</Text>
              <Text style={{ fontWeight: "bold", color: "#4A90E2" }}>
                20 pontos
              </Text>
              <Text style={styles.textDone}>CONCLUÍDO</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
