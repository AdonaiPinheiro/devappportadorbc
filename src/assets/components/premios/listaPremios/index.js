import React, { Component } from "react";
import { FlatList } from "react-native";

//Premios
import Premio from "../premio";

export default class ListaPremios extends Component {
  constructor(props) {
    super(props);

    this.state = {
      listaPremios: [
        {
          id: "1",
          item: "Camiseta Esportiva Comemorativa",
          valor: "999",
          pontos: 2000,
          uri:
            "https://imgcentauro-a.akamaihd.net/900x900/91658101/camiseta-vans-classic-masculina-img.jpg"
        },
        {
          id: "2",
          item: "FIFA 19",
          valor: "100",
          pontos: 20,
          uri:
            "https://i11d.3djuegos.com/juegos/14368/fifa_18/fotos/ficha/fifa_18-3836575.jpg"
        },
        {
          id: "3",
          item: "FIFA 19",
          valor: "100",
          pontos: 20,
          uri:
            "https://i11d.3djuegos.com/juegos/14368/fifa_18/fotos/ficha/fifa_18-3836575.jpg"
        },
        {
          id: "4",
          item: "FIFA 19",
          valor: "100",
          pontos: 20,
          uri:
            "https://imgcentauro-a.akamaihd.net/900x900/91658101/camiseta-vans-classic-masculina-img.jpg"
        },
        {
          id: "5",
          item: "FIFA 19",
          valor: "100",
          pontos: 20,
          uri:
            "https://i11d.3djuegos.com/juegos/14368/fifa_18/fotos/ficha/fifa_18-3836575.jpg"
        },
        {
          id: "6",
          item: "FIFA 19",
          valor: "100",
          pontos: 20,
          uri:
            "https://i11d.3djuegos.com/juegos/14368/fifa_18/fotos/ficha/fifa_18-3836575.jpg"
        },
        {
          id: "7",
          item: "FIFA 19",
          valor: "100",
          pontos: 20,
          uri:
            "https://imgcentauro-a.akamaihd.net/900x900/91658101/camiseta-vans-classic-masculina-img.jpg"
        },
        {
          id: "8",
          item: "FIFA 19",
          valor: "100",
          pontos: 20,
          uri:
            "https://i11d.3djuegos.com/juegos/14368/fifa_18/fotos/ficha/fifa_18-3836575.jpg"
        },
        {
          id: "9",
          item: "FIFA 19",
          valor: "100",
          pontos: 20,
          uri:
            "https://imgcentauro-a.akamaihd.net/900x900/91658101/camiseta-vans-classic-masculina-img.jpg"
        },
        {
          id: "10",
          item: "FIFA 19",
          valor: "100",
          pontos: 20,
          uri:
            "https://i11d.3djuegos.com/juegos/14368/fifa_18/fotos/ficha/fifa_18-3836575.jpg"
        },
        {
          id: "11",
          item: "FIFA 19",
          valor: "100",
          pontos: 20,
          uri:
            "https://imgcentauro-a.akamaihd.net/900x900/91658101/camiseta-vans-classic-masculina-img.jpg"
        },
        {
          id: "12",
          item: "FIFA 19",
          valor: "100",
          pontos: 20,
          uri:
            "https://i11d.3djuegos.com/juegos/14368/fifa_18/fotos/ficha/fifa_18-3836575.jpg"
        },
        {
          id: "13",
          item: "FIFA 19",
          valor: "100",
          pontos: 20,
          uri:
            "https://i11d.3djuegos.com/juegos/14368/fifa_18/fotos/ficha/fifa_18-3836575.jpg"
        },
        {
          id: "14",
          item: "FIFA 19",
          valor: "100",
          pontos: 20,
          uri:
            "https://i11d.3djuegos.com/juegos/14368/fifa_18/fotos/ficha/fifa_18-3836575.jpg"
        },
        {
          id: "15",
          item: "FIFA 19",
          valor: "100",
          pontos: 20,
          uri:
            "https://imgcentauro-a.akamaihd.net/900x900/91658101/camiseta-vans-classic-masculina-img.jpg"
        },
        {
          id: "16",
          item: "FIFA 19",
          valor: "100",
          pontos: 20,
          uri:
            "https://i11d.3djuegos.com/juegos/14368/fifa_18/fotos/ficha/fifa_18-3836575.jpg"
        }
      ],
      listaItems: []
    };
  }

  componentWillMount() {
    let s = this.state;
    if (this.props.search == "") {
      s.listaItems = s.listaPremios;
      this.setState(s);
    } else {
      return;
    }
  }

  render() {
    return (
      <FlatList
        data={this.state.listaItems}
        renderItem={({ item }) => <Premio data={item} />}
        numColumns={2}
        horizontal={false}
        columnWrapperStyle={{ flex: 1, width: "100%" }}
        keyExtractor={(item, index) => item.id}
      />
    );
  }
}
