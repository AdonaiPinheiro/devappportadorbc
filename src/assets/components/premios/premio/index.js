import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  StatusBar,
  ScrollView
} from "react-native";

//Modal
import Modal from "react-native-modalbox";

//Styles
import styles from "./styles";

//Icon
import Icon from "react-native-vector-icons/FontAwesome5";

// id: "2",
//           item: "FIFA 19",
//           valor: "100",
//           pontos: 20,
//           uri:
//             "https://i11d.3djuegos.com/juegos/14368/fifa_18/fotos/ficha/fifa_18-3836575.jpg"

export default class Premio extends Component {
  constructor(props) {
    super(props);

    this.state = {
      iconColor: "#ff4d4d",
      iconSolid: false
    };

    this.selectIcon = this.selectIcon.bind(this);
    this.abrirPremio = this.abrirPremio.bind(this);
    this.cancelar = this.cancelar.bind(this);
  }

  abrirPremio() {
    this.refs.premio.open();
  }

  selectIcon() {
    let s = this.state;
    if (s.iconSolid) {
      s.iconColor = "#ff4d4d";
      s.iconSolid = false;
    } else {
      (s.iconColor = "#ff4d4d"), (s.iconSolid = true);
    }

    this.setState(s);
  }

  cancelar() {
    this.refs.resgatar.close();
  }

  componentWillMount() {
    if (this.props.data.item.lenght > 10) {
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={{ flexDirection: "row" }}
          onPress={() => {
            this.abrirPremio();
          }}
        >
          <View
            style={{
              zIndex: 1,
              justifyContent: "space-between",
              position: "relative"
            }}
          >
            <View>
              <Text style={styles.textTitulo}>{this.props.data.item}</Text>
            </View>
            <View>
              <View style={styles.valor}>
                <View style={styles.viewIcon}>
                  <Icon name="dollar-sign" size={10} color="#FFF" />
                </View>
                <Text style={styles.text}>R$ {this.props.data.valor}</Text>
              </View>
              <View style={styles.pontos}>
                <View style={styles.viewIcon}>
                  <Icon name="star" size={10} color="#FFF" />
                </View>
                <Text style={styles.text}>{this.props.data.pontos} pts</Text>
              </View>
            </View>
          </View>
          <View style={{ marginLeft: 5, zIndex: 0, position: "relative" }}>
            <Image style={styles.img} source={{ uri: this.props.data.uri }} />
          </View>
          <View style={styles.viewLike}>
            <TouchableOpacity
              onPress={() => {
                this.selectIcon();
              }}
            >
              <Icon
                name="heart"
                size={20}
                color={this.state.iconColor}
                solid={this.state.iconSolid}
              />
            </TouchableOpacity>
          </View>
        </TouchableOpacity>

        <Modal
          style={styles.modal}
          position={"bottom"}
          ref={"premio"}
          entry={"bottom"}
          coverScreen={true}
          animationDuration={600}
          backdropColor={"#CCC"}
          backButtonClose={true}
        >
          <StatusBar barStyle="dark-content" backgroundColor="#CCC" />
          <View style={{ flex: 1, width: "100%" }}>
            <View style={styles.modalExit}>
              <Text style={styles.txtExitModal}>
                Clique ou arraste para fechar
              </Text>
            </View>
            <View style={styles.modalBody}>
              <Text style={styles.txtTituloModal}>{this.props.data.item}</Text>

              <View style={styles.modalInfos}>
                <Image
                  style={styles.imgModal}
                  source={{ uri: this.props.data.uri }}
                />
                <View
                  style={{
                    width: "70%",
                    marginTop: -50,
                    alignSelf: "center"
                  }}
                >
                  <View
                    style={{
                      padding: 5,
                      minWidth: 110,
                      backgroundColor: "#4A90E2",
                      alignSelf: "flex-end",
                      borderTopLeftRadius: 5,
                      flexDirection: "row"
                    }}
                  >
                    <Icon name="dollar-sign" color="#FFF" size={16} />
                    <Text style={{ marginLeft: 5, color: "#FFF" }}>
                      R$ {this.props.data.valor}
                    </Text>
                  </View>
                  <View
                    style={{
                      padding: 5,
                      minWidth: 110,
                      backgroundColor: "#50E3C2",
                      alignSelf: "flex-end",
                      borderBottomRightRadius: 5,
                      flexDirection: "row"
                    }}
                  >
                    <Icon name="star" color="#FFF" size={16} />
                    <Text style={{ marginLeft: 5, color: "#FFF" }}>
                      {this.props.data.pontos} pontos
                    </Text>
                  </View>
                </View>
              </View>
              <View>
                <Text style={{ textAlign: "center" }}>
                  Novo jogo FIFA 19 da EA GAMES! Disponível apenas no posto de
                  Campinas - SP
                </Text>
              </View>
              <View style={{ width: "80%" }}>
                <TouchableOpacity
                  onPress={() => {
                    this.refs.resgatar.open();
                  }}
                  style={styles.btnResgate}
                >
                  <Icon name="shopping-cart" color="#FFF" size={24} />
                  <Text style={styles.txtResgate}>RESGATAR PRODUTO</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>

        <Modal
          style={styles.modalResgatar}
          position={"center"}
          ref={"resgatar"}
          entry={"bottom"}
          coverScreen={true}
          animationDuration={600}
          backdropColor={"#CCC"}
          backButtonClose={true}
        >
          <StatusBar barStyle="dark-content" backgroundColor="#CCC" />
          <View
            style={{
              flex: 1,
              width: "100%",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <View style={{ alignItems: "center", justifyContent: "center" }}>
              <Text style={{ fontSize: 14, fontWeight: "bold" }}>
                Confirmar?
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginTop: 10
              }}
            >
              <TouchableOpacity
                style={{
                  margin: 5,
                  width: 50,
                  height: 50,
                  borderRadius: 25,
                  backgroundColor: "#ff4d4d",
                  alignItems: "center",
                  justifyContent: "center"
                }}
                onPress={() => {
                  this.cancelar();
                }}
              >
                <Icon name="times" size={24} color="#FFF" />
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  margin: 5,
                  width: 50,
                  height: 50,
                  borderRadius: 25,
                  backgroundColor: "#50E3C2",
                  alignItems: "center",
                  justifyContent: "center"
                }}
                onPress={() => {
                  this.refs.resgatar.close();
                  this.refs.premio.close();
                  this.refs.result.open();
                }}
              >
                <Icon name="check" size={24} color="#FFF" />
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <Modal
          style={styles.modal}
          position={"center"}
          ref={"result"}
          entry={"bottom"}
          coverScreen={true}
          animationDuration={600}
          backdropColor={"#CCC"}
          backdropPressToClose={false}
          swipeToClose={false}
        >
          <StatusBar barStyle="dark-content" backgroundColor="#CCC" />
          <View
            style={{
              flex: 1,
              width: "100%",
              alignItems: "center",
              justifyContent: "space-between",
              marginTop: 50,
              margin: 20
            }}
          >
            <View>
              <Image
                style={[styles.imgModal, { width: 100, height: 120 }]}
                source={{ uri: this.props.data.uri }}
              />
              <Text style={{ fontWeight: "bold" }}>{this.props.data.item}</Text>
            </View>
            <ScrollView style={{ flex: 1, width: "80%", margin: 20 }}>
              <View style={{ marginBottom: 20 }}>
                <Text style={{ color: "#F5A623", fontWeight: "bold" }}>
                  Resgate efetuado
                </Text>
                <Text style={{ color: "#F5A623", textAlign: "justify" }}>
                  IMPORTANTE: Seu resgate estará disponivel por 48h, efetue o
                  pagamento junto ao lojista para retirar seu prêmio. Se for um
                  cupom de desconto ele terá validade por até 30 dias.
                </Text>
              </View>
              <View style={{ marginBottom: 20 }}>
                <Text style={{ color: "#D0021B", fontWeight: "bold" }}>
                  Erro ao resgatar
                </Text>
                <Text style={{ color: "#D0021B", textAlign: "justify" }}>
                  IMPORTANTE: Houve um erro ao resgatar seu produto, tente
                  realizar a ação novamente. Caso o erro persista, entre em
                  contato com nossa central de atendimento.
                </Text>
              </View>
              <View>
                <Text>Data: 21/01/2019</Text>
                <Text>Nome: FIFA 19</Text>
                <Text>Cód. do resgate 1210122310000</Text>
                <Text>Código do Produto: 135</Text>
                <Text>Quantidade: 01 unidade</Text>
                <Text>Pontos: 200</Text>
                <Text>Valor: 100,00</Text>
                <Text>Nome: PEDRO ROCHA</Text>
                <Text>CPF: 93888165016</Text>
                <Text>Nº do cartão: 6371804435110010</Text>
                <Text>E-mail: pedro.ferreira@braziliancard.com.br</Text>
              </View>
            </ScrollView>
            <View style={{ width: "80%" }}>
              <TouchableOpacity
                onPress={() => {
                  this.refs.result.close();
                }}
                style={[styles.btnResgate, { backgroundColor: "#001f4d" }]}
              >
                <Text style={[styles.txtResgate, { color: "#fff" }]}>
                  CONTINUAR
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}
