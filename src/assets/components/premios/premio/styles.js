import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 100,
    width: "100%",
    margin: 2.5,
    flexDirection: "row",
    borderColor: "#1212",
    borderWidth: 0.5,
    borderRadius: 5,
    backgroundColor: "transparent"
  },
  valor: {
    minWidth: 85,
    maxWidth: 90,
    backgroundColor: "#4A90E2",
    alignItems: "center",
    justifyContent: "flex-start",
    flexDirection: "row",
    borderTopRightRadius: 5,
    padding: 1,
    height: 20,
    paddingRight: 5
  },
  pontos: {
    minWidth: 85,
    maxWidth: 90,
    backgroundColor: "#50E3C2",
    alignItems: "center",
    justifyContent: "flex-start",
    flexDirection: "row",
    borderBottomLeftRadius: 5,
    padding: 1,
    height: 20,
    paddingRight: 5
  },
  text: {
    marginLeft: 2.5,
    color: "#FFF",
    fontWeight: "bold",
    fontSize: 12
  },
  textTitulo: {
    fontWeight: "bold",
    marginLeft: 5,
    marginTop: 2.5,
    maxWidth: 80,
    fontSize: 12
  },
  viewIcon: {
    margin: 5,
    justifyContent: "center",
    alignItems: "center",
    width: 15,
    height: 15,
    borderWidth: 1,
    borderColor: "#FFF",
    borderRadius: 7.5
  },
  viewLike: {
    marginBottom: 2,
    marginLeft: -25,
    zIndex: 2,
    justifyContent: "flex-end"
  },
  img: {
    width: 75,
    height: "100%",
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
    marginLeft: -1
  },
  //ModalSettings
  modal: {
    flex: 1,
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  modalExit: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    padding: 5,
    marginTop: 10
  },
  txtExitModal: {
    fontStyle: "normal",
    fontSize: 12,
    textAlign: "center"
  },
  modalBody: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-between",
    margin: 10,
    borderRadius: 10,
    backgroundColor: "transparent"
  },
  modalInfos: {
    width: "100%"
  },
  txtTituloModal: {
    fontSize: 18,
    margin: 10,
    fontWeight: "bold"
  },
  imgModal: {
    width: 150,
    height: 175,
    borderRadius: 5,
    alignSelf: "center"
  },
  txtResgate: {
    color: "#FFF",
    marginLeft: 10
  },
  btnResgate: {
    width: "100%",
    height: 50,
    backgroundColor: "#F5A623",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
    marginBottom: 20
  },
  modalResgatar: {
    width: 200,
    height: 200,
    backgroundColor: "#FFF",
    borderRadius: 15
  }
});

export default styles;
