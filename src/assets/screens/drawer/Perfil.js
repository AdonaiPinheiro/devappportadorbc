import React, { Component } from "react";
import {
  Text,
  TouchableHighlight,
  View,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
  Image
} from "react-native";
import { TextInputMask } from "react-native-masked-text";

//Components
import Header from "../../components/screens/Header";

//Styles
import styles from "../../styles/pagsDrawer";

//Img Profile
const imgProfile = {
  uri:
    "https://media.licdn.com/dms/image/C4D03AQFRFVoQczXYjQ/profile-displayphoto-shrink_200_200/0?e=1563408000&v=beta&t=qdgoicpd-6X_HyW_GQexu15qbGoesFZZHptQ6g8hrtU"
};

export default class Perfil extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tel: "",
      email: "",
      senha: "",
      novaSenha: "",
      checkSenha: "",
      colorBtn: "#ccc",
      colorBorder: "#4A4A4A",
      widthBorder: 0.3
    };

    this.checkSave = this.checkSave.bind(this);
  }

  checkSave(val) {
    if (val === this.state.novaSenha) {
      let s = this.state;
      s.colorBtn = "#001f4d";
      s.colorBorder = "#5cd65c";
      this.setState(s);
    } else if (val == "") {
      let s = this.state;
      s.colorBtn = "#ccc";
      s.colorBorder = "#4A4A4A";
      s.widthBorder = 0.3;
    } else {
      let s = this.state;
      s.colorBtn = "#ccc";
      s.colorBorder = "#ff4d4d";
      s.widthBorder = 2;
      this.setState(s);
    }
  }

  render() {
    return (
      <KeyboardAvoidingView style={[styles.container]} behavior="height" enable>
        <ScrollView style={{ flex: 1 }}>
          <View style={[styles.header, { marginTop: 6 }]}>
            <Header size={24} />
          </View>
          <View
            style={[styles.body, { marginVertical: 15, paddingHorizontal: 10 }]}
          >
            <View
              style={{
                width: 125,
                height: 125,
                alignSelf: "center",
                marginBottom: 20
              }}
            >
              <Image
                source={imgProfile}
                style={{
                  width: 125,
                  height: 125,
                  borderRadius: 62.5,
                  borderWidth: 5,
                  borderColor: "#001f4d90"
                }}
              />
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.textTitulo}>Nome: </Text>
              <Text style={styles.textBody}>Adonai Junio Pinheiro</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.textTitulo}>CPF: </Text>
              <Text style={styles.textBody}>123.456.678-10</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.textTitulo}>Últimos números do cartão: </Text>
              <Text style={styles.textBody}>2735</Text>
            </View>
            <View style={{ marginTop: 10 }}>
              <Text style={styles.textTitulo}>Telefone</Text>
              <TextInputMask
                type={"custom"}
                options={{
                  mask: "(99) 9 9999-9999"
                }}
                textContentType={"telephoneNumber"}
                style={styles.inputPerfil}
                placeholder="Digite seu número de telefone"
                value={this.state.tel}
                onChangeText={val => {
                  this.setState({ tel: val });
                }}
              />
            </View>
            <View style={{ marginTop: 10 }}>
              <Text style={styles.textTitulo}>E-mail</Text>
              <TextInput
                style={styles.inputPerfil}
                placeholder="adonaijpinheiro@gmail.com"
                value={this.state.email}
                onChangeText={val => {
                  this.setState({ email: val });
                }}
              />
            </View>
            <View style={{ marginTop: 10 }}>
              <Text style={styles.textTitulo}>Alteração de Senha</Text>
              <TextInput
                style={[
                  styles.inputPerfil,
                  {
                    borderColor: this.state.colorBorder,
                    borderWidth: this.state.widthBorder
                  }
                ]}
                placeholder="Digite a senha antiga"
                secureTextEntry={true}
                value={this.state.senha}
                onChangeText={val => {
                  this.setState({ senha: val });
                }}
              />
              <TextInput
                style={[
                  styles.inputPerfil,
                  {
                    borderColor: this.state.colorBorder,
                    borderWidth: this.state.widthBorder
                  }
                ]}
                placeholder="Nova senha"
                secureTextEntry={true}
                value={this.state.novaSenha}
                onChangeText={val => {
                  this.setState({ novaSenha: val });
                }}
              />
              <TextInput
                style={[
                  styles.inputPerfil,
                  {
                    borderColor: this.state.colorBorder,
                    borderWidth: this.state.widthBorder
                  }
                ]}
                placeholder="Confirmar senha"
                secureTextEntry={true}
                value={this.state.checkSenha}
                onChangeText={val => {
                  this.checkSave(val);
                  this.setState({ checkSenha: val });
                }}
              />
            </View>
            <View style={{ marginTop: 10 }}>
              <TouchableHighlight
                underlayColor="#99e699"
                style={[
                  styles.saveButton,
                  {
                    backgroundColor: this.state.colorBtn,
                    borderColor: this.state.colorBtn
                  }
                ]}
                onPress={() => {
                  this.checkSave;
                }}
              >
                <Text style={styles.textButtonSave}>SALVAR</Text>
              </TouchableHighlight>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}
