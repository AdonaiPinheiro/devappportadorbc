import React, { Component } from "react";
import { Text, View, ScrollView, TouchableHighlight } from "react-native";
import { withNavigation } from "react-navigation";

//Components
import Header from "../../components/screens/Header";

//Icons
import Icon from "react-native-vector-icons/FontAwesome5";

//Styles
import styles from "../../styles/pagsDrawer";

export class Termos extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Header size={24} />
        </View>
        <View style={[styles.body, { paddingHorizontal: 10 }]}>
          <ScrollView showsVerticalScrollIndicator={true}>
            <Text
              style={[
                styles.textTitulo,
                { alignSelf: "center", marginBottom: 10 }
              ]}
            >
              Termos de Uso
            </Text>
            <Text style={styles.textBody}>
              {"     "}Lorem ipsum dolor sit amet, consectetur adipiscing elit,
              sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur. Excepteur sint occaecat cupidatat non proident,
              sunt in culpa qui officia deserunt mollit anim id est laborum.
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud
            </Text>
            <Text style={styles.textBody}>
              {"     "}
              Exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate
              velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
              occaecat cupidatat non proident, sunt in culpa qui officia
              deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet,
              consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
              labore et dolore magna aliqua. Ut enim ad minim veniam, quis
              nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate
              velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
              occaecat cupidatat non proident, sunt in culpa qui officia
              deserunt mollit anim id est laborum.
            </Text>
          </ScrollView>
        </View>
      </View>
    );
  }
}

export default withNavigation(Termos);
