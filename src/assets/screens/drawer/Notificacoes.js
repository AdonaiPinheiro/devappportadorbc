import React, { Component } from "react";
import { Text, View, ScrollView, TouchableHighlight } from "react-native";
import { withNavigation } from "react-navigation";

//Components
import Header from "../../components/screens/Header";

//Styles
import styles from "../../styles/pagsDrawer";

export class Notificacoes extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Header size={24} />
        </View>
        <View style={styles.body}>
          <ScrollView />
        </View>
      </View>
    );
  }
}

export default withNavigation(Notificacoes);
