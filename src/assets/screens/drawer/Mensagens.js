import React, { Component } from "react";
import { View, KeyboardAvoidingView, ScrollView } from "react-native";
import { withNavigation } from "react-navigation";

//Components
import Header from "../../components/screens/Header";
import Writer from "../../components/mensagens/writer";
import ListaMensagens from "../../components/mensagens/listMsg";

//Styles
import styles from "../../styles/pagsDrawer";

export class Mensagens extends Component {
  render() {
    return (
      <KeyboardAvoidingView
        enabled
        behavior="height"
        style={[styles.container, { marginTop: -1 }]}
      >
        <View style={styles.header}>
          <Header size={24} />
        </View>

        <View
          style={[
            styles.body,
            {
              justifyContent: "space-between",
              marginBottom: 10,
              paddingTop: 5
            }
          ]}
        >
          <ListaMensagens />
          <Writer />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

export default withNavigation(Mensagens);
