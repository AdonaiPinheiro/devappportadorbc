import React, { Component } from "react";
import { Text, View, ScrollView, TouchableHighlight } from "react-native";
import { withNavigation } from "react-navigation";
import Accordion from "react-native-collapsible/Accordion";

//Components
import Header from "../../components/screens/Header";

//Icons
import Icon from "react-native-vector-icons/FontAwesome5";

//Styles
import styles from "../../styles/pagsDrawer";

const SECTIONS = [
  {
    FAQheader: "",
    FAQtitle: "O que acontece se...",
    FAQcontent:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean at est eu urna interdum porta. Nulla maximus purus non magna tristique, sed efficitur lectus sollicitudin. Proin fringilla aliquet tellus quis fermentum. Fusce placerat ligula ac nunc faucibus, non condimentum ante suscipit. Nam id fermentum ante. Quisque et nisl ex. Vestibulum egestas tristique sodales."
  },
  {
    FAQheader: "",
    FAQtitle: "Se o cartão...",
    FAQcontent:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean at est eu urna interdum porta. Nulla maximus purus non magna tristique, sed efficitur lectus sollicitudin. Proin fringilla aliquet tellus quis fermentum. Fusce placerat ligula ac nunc faucibus, non condimentum ante suscipit. Nam id fermentum ante. Quisque et nisl ex. Vestibulum egestas tristique sodales."
  },
  {
    FAQheader: "",
    FAQtitle: "Quero meu crédito de volta",
    FAQcontent:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean at est eu urna interdum porta. Nulla maximus purus non magna tristique, sed efficitur lectus sollicitudin. Proin fringilla aliquet tellus quis fermentum. Fusce placerat ligula ac nunc faucibus, non condimentum ante suscipit. Nam id fermentum ante. Quisque et nisl ex. Vestibulum egestas tristique sodales."
  },
  {
    FAQheader: "",
    FAQtitle: "Como resgatar meu prémio?",
    FAQcontent:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean at est eu urna interdum porta. Nulla maximus purus non magna tristique, sed efficitur lectus sollicitudin. Proin fringilla aliquet tellus quis fermentum. Fusce placerat ligula ac nunc faucibus, non condimentum ante suscipit. Nam id fermentum ante. Quisque et nisl ex. Vestibulum egestas tristique sodales."
  }
];

export class Ajuda extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeSections: []
    };
  }

  _renderSectionTitle = section => {
    return (
      <View style={styles.FAQheader}>
        <Text>{section.FAQheader}</Text>
      </View>
    );
  };

  _renderHeader = section => {
    return (
      <View style={styles.FAQtitle}>
        <Text style={styles.FAQtitleText}>{section.FAQtitle}</Text>
      </View>
    );
  };

  _renderContent = section => {
    return (
      <View style={styles.FAQcontent}>
        <Text style={styles.FAQcontentText}>{section.FAQcontent}</Text>
      </View>
    );
  };

  _updateSections = activeSections => {
    this.setState({ activeSections });
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Header size={24} />
        </View>

        <View style={styles.body}>
          <ScrollView>
            <Accordion
              sections={SECTIONS}
              activeSections={this.state.activeSections}
              renderSectionTitle={this._renderSectionTitle}
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
              onChange={this._updateSections}
              underlayColor="#CCC"
            />
          </ScrollView>
        </View>
      </View>
    );
  }
}

export default withNavigation(Ajuda);
