import React, { Component } from "react";
import { View, Image, ActivityIndicator } from "react-native";

const img = require("../../../images/icon_splash.png");

export default class Preload extends Component {
  constructor(props) {
    super(props);

    this.state = { user: 0 };
    //Verificar aqui se usuário está conectado ou não

    if (this.state.user === 1) {
      this.props.navigation.navigate("Main");
    } else {
      this.props.navigation.navigate("Login");
    }
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "#fff"
        }}
      >
        <Image style={{ width: 200, height: 200 }} source={img} />
        <ActivityIndicator size="large" color="#001433" />
      </View>
    );
  }
}
