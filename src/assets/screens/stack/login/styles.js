import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  header: {
    flex: 0.12,
    alignItems: "center",
    justifyContent: "center"
  },
  body: {
    flex: 0.88,
    marginLeft: 10,
    marginRight: 10,
    padding: 10,
    alignSelf: "stretch"
  },
  buttonArea: {
    flexDirection: "row",
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 10,
    marginTop: 5,
    marginBottom: 5,
    borderColor: "#CCC",
    borderWidth: 0.3,
    borderRadius: 30,
    width: "100%"
  },
  buttonText: {
    fontWeight: "bold",
    marginRight: "25%"
  },
  textInput: {
    alignItems: "center",
    justifyContent: "center",
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 5,
    marginBottom: 5,
    borderColor: "#CCC",
    borderWidth: 0.3,
    borderRadius: 5,
    width: "100%",
    height: 40
  },
  //Modal
  barra: {
    height: 3,
    width: 75,
    borderRadius: 6,
    backgroundColor: "#000",
    alignSelf: "center",
    margin: 10
  },
  modal: {
    justifyContent: "center",
    alignItems: "center",
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
    padding: 10
  },
  modal4: {
    height: "50%",
    minHeight: 300
  },
  text: {
    color: "black",
    fontSize: 22
  }
});

export default styles;
