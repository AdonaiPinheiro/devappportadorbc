import React, { Component } from "react";
import {
  Text,
  View,
  ScrollView,
  StatusBar,
  Image,
  TouchableOpacity,
  TextInput,
  Picker,
  ActivityIndicator,
  Dimensions
} from "react-native";
import { withNavigation } from "react-navigation";

//Modal
import Modal from "react-native-modalbox";

//Icons
import Icon from "react-native-vector-icons/FontAwesome5";

//Components
import Header from "../../../components/screens/Header";

//Dimensions
const { height } = Dimensions.get("window");

//Styles
import styles from "./styles";

export class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      senha: "",
      loja: "Loja",
      digCartao: "",
      isOpen: false,
      isDisabled: false,
      swipeToClose: true
    };

    this.login = this.login.bind(this);
    this.loading = this.loading.bind(this);
    this.abrirModalEmail = this.abrirModalEmail.bind(this);
    this.abrirModalCartao = this.abrirModalCartao.bind(this);
  }

  loginGoogle() {}

  loginFb() {}

  loginEmail() {}

  loginCartao() {}

  abrirModalEmail() {
    this.refs.email.open();
  }

  abrirModalCartao() {
    this.refs.cartao.open();
  }

  async login() {
    await this.refs.cartao.close();
    await this.refs.email.close();
    await this.loading();
  }

  async loading() {
    await this.refs.loading.open();
    this.props.navigation.navigate("Main");
  }

  render() {
    return (
      <ScrollView scrollEnabled={false} style={styles.container}>
        <View style={[styles.header, { marginTop: 30 }]}>
          <Header size={0} menu={0} />
        </View>
        <View style={styles.body}>
          <View
            style={{
              flex: 1,
              height: height - 200,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <TouchableOpacity
              style={[styles.buttonArea, { backgroundColor: "#FFF" }]}
              onPress={this.loginGoogle}
            >
              <Image
                style={{ width: 28, height: 28 }}
                source={require("../../../images/social/google.png")}
              />
              <Text style={[styles.buttonText, { color: "#1219" }]}>
                Entrar com o Google
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.buttonArea, { backgroundColor: "#3C5B9A" }]}
              onPress={this.loginFb}
            >
              <Image
                style={{ width: 28, height: 28 }}
                source={require("../../../images/social/fb.png")}
              />
              <Text style={[styles.buttonText, { color: "#FFF" }]}>
                Entrar com o Facebook
              </Text>
            </TouchableOpacity>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                marginTop: 25,
                marginBottom: 25
              }}
            >
              <View style={{ height: 1, width: 50, backgroundColor: "#ccc" }} />
              <Text style={{ margin: 5 }}>OU</Text>
              <View style={{ height: 1, width: 50, backgroundColor: "#ccc" }} />
            </View>
            <TouchableOpacity
              style={[styles.buttonArea, { backgroundColor: "#FFF" }]}
              onPress={this.abrirModalEmail}
            >
              <Icon name="envelope" size={28} color="#1219" light />
              <Text style={[styles.buttonText, { color: "#1219" }]}>
                Entrar com E-mail
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.buttonArea, { backgroundColor: "#FFF" }]}
              onPress={this.abrirModalCartao}
            >
              <Icon name="credit-card" size={24} color="#1219" light />
              <Text style={[styles.buttonText, { color: "#1219" }]}>
                Entrar com Cartão
              </Text>
            </TouchableOpacity>
          </View>

          {/* { Começa os modals } */}

          <Modal
            style={[styles.modal, styles.modal4]}
            position={"bottom"}
            ref={"email"}
            entry={"bottom"}
            coverScreen={true}
            animationDuration={400}
            backdropColor={"#CCC"}
            backButtonClose={true}
          >
            <StatusBar barStyle="dark-content" backgroundColor="#CCC" />
            <Text>Entrar com e-mail</Text>
            <TextInput
              placeholder="Digite seu e-mail"
              onChangeText={email => {
                this.setState({ email: email });
              }}
              value={this.state.email}
              style={styles.textInput}
            />
            <View style={styles.textInput}>
              <Picker
                selectedValue={this.state.loja}
                style={{ width: "100%", paddingLeft: 0, paddingRight: 0 }}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({ loja: itemValue })
                }
              >
                <Picker.Item label="Loja" value="loja" />
                <Picker.Item label="JavaScript" value="js" />
                <Picker.Item label="Teste" value="teste" />
                <Picker.Item label="Adonai" value="adonai" />
              </Picker>
            </View>

            <TextInput
              secureTextEntry={true}
              placeholder="Digite sua senha"
              onChangeText={senha => {
                this.setState({ senha: senha });
              }}
              value={this.state.senha}
              style={styles.textInput}
            />
            <TouchableOpacity
              style={[
                styles.buttonArea,
                {
                  backgroundColor: "#001f4d",
                  justifyContent: "center",
                  borderRadius: 7,
                  height: 40,
                  flexDirection: "column"
                }
              ]}
              onPress={this.login}
            >
              <Text
                style={[styles.buttonText, { color: "#FFF", marginRight: 0 }]}
              >
                Entrar
              </Text>
            </TouchableOpacity>
            <Text
              style={{
                color: "#1215",
                textAlign: "center",
                justifyContent: "flex-end"
              }}
            >
              Esqueceu a senha? Clique aqui.
            </Text>
          </Modal>

          <Modal
            style={[styles.modal, styles.modal4]}
            position={"bottom"}
            ref={"cartao"}
            entry={"bottom"}
            coverScreen={true}
            animationDuration={400}
            backdropColor={"#CCC"}
            backButtonClose={true}
          >
            <StatusBar barStyle="dark-content" backgroundColor="#CCC" />
            <Text>Entrar com cartão</Text>
            <TextInput
              placeholder="8 últimos números do seu cartão"
              keyboardType="number-pad"
              onChangeText={digCartao => {
                this.setState({ digCartao: digCartao });
              }}
              value={this.state.digCartao}
              style={styles.textInput}
            />

            <TextInput
              secureTextEntry={true}
              placeholder="Digite sua senha"
              onChangeText={senha => {
                this.setState({ senha: senha });
              }}
              value={this.state.senha}
              style={styles.textInput}
            />
            <TouchableOpacity
              style={[
                styles.buttonArea,
                {
                  backgroundColor: "#001f4d",
                  justifyContent: "center",
                  borderRadius: 7,
                  height: 40,
                  flexDirection: "column"
                }
              ]}
              onPress={this.login}
            >
              <Text
                style={[styles.buttonText, { color: "#FFF", marginRight: 0 }]}
              >
                Entrar
              </Text>
            </TouchableOpacity>
            <Text
              style={{
                color: "#1215",
                textAlign: "center",
                justifyContent: "flex-end"
              }}
            >
              Esqueceu a senha? Clique aqui.
            </Text>
          </Modal>

          <Modal
            style={[
              styles.modal,
              styles.modal4,
              {
                borderRadius: 15,
                width: 200,
                height: 200,
                maxHeight: 200,
                minHeight: 200
              }
            ]}
            position={"center"}
            ref={"loading"}
            entry={"bottom"}
            coverScreen={true}
            animationDuration={400}
            backdropColor={"#CCC"}
          >
            <StatusBar barStyle="dark-content" backgroundColor="#CCC" />
            <ActivityIndicator size="small" color="#CCC" />
            <Text
              style={{
                color: "#1215",
                textAlign: "center",
                justifyContent: "flex-end"
              }}
            >
              Carregando...
            </Text>
          </Modal>
        </View>
      </ScrollView>
    );
  }
}

export default withNavigation(Login);
