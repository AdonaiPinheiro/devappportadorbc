import React, { Component } from "react";
import { Text, View, ScrollView, KeyboardAvoidingView } from "react-native";
import { withNavigation } from "react-navigation";

//Components
import Header from "../../components/screens/Header";
import Dropdown from "../../components/screens/dropdown";
import Search from "../../components/screens/search";
import Resgates from "../../components/premios/resgates";
import ListaPremios from "../../components/premios/listaPremios";

//Styles
import styles from "../../styles/pagsDrawer";

export class Premios extends Component {
  constructor(props) {
    super(props);

    this.state = {
      search: ""
    };
  }

  render() {
    return (
      <KeyboardAvoidingView behavior="height" enabled style={styles.container}>
        <View style={styles.header}>
          <Header size={0} />
        </View>
        <View
          style={[
            styles.body,
            {
              justifyContent: "space-between"
            }
          ]}
        >
          <Dropdown />
          <Search />
          <ListaPremios search="" />

          <Resgates />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

export default withNavigation(Premios);
