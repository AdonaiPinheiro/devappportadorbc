import React, { Component } from "react";
import { View, ScrollView, Text, ActivityIndicator, Image } from "react-native";
import { withNavigation } from "react-navigation";

//Components
import Header from "../../components/screens/Header";
import CreditoMain from "../../components/home/credito";
import DebitoMain from "../../components/home/debito";
import PontosMain from "../../components/home/pontos";
import ClubeMain from "../../components/home/clube";
import Dropdown from "../../components/screens/dropdown";
import HomeSwiper from "../../components/home/swiper";

//Swiper
import Swiper from "react-native-swiper";

//Styles
import styles from "../../styles/pagsDrawer";

//Config Swiper
const NextButton = () => (
  <Text
    style={{
      color: "#FFF",
      fontWeight: "bold",
      fontSize: 36
    }}
  >
    ›
  </Text>
);
const PrevButton = () => (
  <Text
    style={{
      color: "#FFF",
      fontWeight: "bold",
      fontSize: 36
    }}
  >
    ‹
  </Text>
);
const Dot = () => (
  <View
    style={{
      backgroundColor: "rgba(0,0,0,.2)",
      width: 8,
      height: 8,
      borderRadius: 4,
      marginLeft: 3,
      marginRight: 3,
      marginTop: 3,
      marginBottom: 3
    }}
  />
);
const ActiveDot = () => (
  <View
    style={{
      backgroundColor: "#001f4d",
      width: 8,
      height: 8,
      borderRadius: 4,
      marginLeft: 3,
      marginRight: 3,
      marginTop: 3,
      marginBottom: 3
    }}
  />
);
//End Config Swiper

//Image
const img = require("../../../assets/images/icon_splash.png");

export class Inicio extends Component {
  componentWillMount() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "#fff"
        }}
      >
        <Image style={{ width: 200, height: 200 }} source={img} />
        <ActivityIndicator size="large" color="#001433" />
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Header size={0} />
        </View>
        <View style={styles.body}>
          <Dropdown />
          <View
            style={{
              flex: 0.95,
              justifyContent: "space-between"
            }}
          >
            <Swiper
              style={styles.areaSwiper}
              autoplay={false}
              showsButtons={true}
              paginationStyle={{
                bottom: -45,
                center: 10
              }}
              nextButton={<NextButton />}
              prevButton={<PrevButton />}
              loop={false}
              dot={<Dot />}
              activeDot={<ActiveDot />}
              buttonWrapperStyle={{ marginTop: -130 }}
            >
              <CreditoMain />
              <DebitoMain />
              <PontosMain />
              <ClubeMain />
            </Swiper>

            <HomeSwiper />
          </View>
        </View>
      </View>
    );
  }
}

export default withNavigation(Inicio);
