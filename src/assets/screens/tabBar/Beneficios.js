import React, { Component } from "react";
import { Text, View, ScrollView, Picker } from "react-native";
import { withNavigation } from "react-navigation";

//Components
import Header from "../../components/screens/Header";
import Dropdown from "../../components/screens/dropdown";
import ListaEstabelecimentos from "../../components/estabelecimentos/list";
import ListaBeneficios from "../../components/benUtilizados/list";

//Icons
import Icon from "react-native-vector-icons/FontAwesome5";

//Styles
import styles from "../../styles/pagsDrawer";

export class Beneficios extends Component {
  constructor(props) {
    super(props);

    this.state = {
      categoria: ""
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Header size={0} />
        </View>
        <View style={styles.body}>
          <Dropdown />
          <View
            style={{
              width: "100%",
              height: 40,
              borderRadius: 5,
              borderColor: "#CCC",
              borderWidth: 1,
              alignItems: "center",
              justifyContent: "center",
              paddingHorizontal: 10,
              marginBottom: 10
            }}
          >
            <Picker
              selectedValue={this.state.categoria}
              mode="dropdown"
              style={{
                width: "100%"
              }}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({ categoria: itemValue })
              }
            >
              <Picker.Item label="Todas as categorias" value="default" />
              <Picker.Item label="JavaScript" value="js" />
              <Picker.Item label="Teste" value="teste" />
              <Picker.Item label="Adonai" value="adonai" />
            </Picker>
          </View>
          <ScrollView>
            <View style={{ alignItems: "center", marginTop: 10 }}>
              <Text style={{ fontSize: 18 }}>Estabelecimentos disponíveis</Text>
            </View>

            <View>
              <ListaEstabelecimentos />
            </View>

            <View style={{ marginTop: 10 }}>
              <View
                style={{ flexDirection: "row", marginLeft: 10, marginTop: 10 }}
              >
                <Icon name="user-friends" size={15} color="#4A90E2" />
                <Text style={{ marginLeft: 5, color: "#4A90E2", fontSize: 12 }}>
                  BENEFÍCIOS UTILIZADOS
                </Text>
              </View>
              <View
                style={{
                  borderWidth: 1,
                  borderColor: "#CCC",
                  borderRadius: 10,
                  padding: 10,
                  marginVertical: 10
                }}
              >
                <ListaBeneficios />
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

export default withNavigation(Beneficios);
