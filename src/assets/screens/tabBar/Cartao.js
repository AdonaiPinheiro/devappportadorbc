import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { withNavigation } from "react-navigation";

//Components
import Header from "../../components/screens/Header";
import CartaoForm from "../../components/cartao";
import Dropdown from "../../components/screens/dropdown";

//Styles
import styles from "../../styles/pagsDrawer";

{
  /* <CartaoForm
            qrcode={this.state.qrcode}
            nome={this.state.nome}
            numCartao={this.state.numCartao}
            val={this.state.val}
          /> */
}

export class Cartao extends Component {
  constructor(props) {
    super(props);

    this.state = {
      qrcode: "https://www.braziliancard.com.br/",
      nome: "PEDRO ROCHA",
      numCartao: "6371 8044 3511 0010",
      val: "09/21"
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Header size={0} />
        </View>
        <View
          style={[
            styles.body,
            {
              alignItems: "center",
              justifyContent: "space-between"
            }
          ]}
        >
          <Dropdown />
          <CartaoForm
            qrcode={this.state.qrcode}
            nome={this.state.nome}
            numCartao={this.state.numCartao}
            val={this.state.val}
          />
        </View>
      </View>
    );
  }
}

export default withNavigation(Cartao);
