<h1 align="center">
  Dev App Portador BC
</h1>

<p align="center">
  <strong>Repositório para centralizar o Ambiente de Desenvolvimento</strong>
  <p align="center">
    <img src="https://ci.appveyor.com/api/projects/status/g8d58ipi3auqdtrk/branch/master?svg=true" alt="Current Appveyor build status." />
    <img src="https://img.shields.io/badge/react--native-0.59.5-blue.svg" alt="Current RN package version." />
    <img src="https://img.shields.io/badge/version-1.0-brightgreen.svg" alt="Current APP version." />  
  </p>
</p>

## 📋 Briefing

## 📖 Requirements
```
"react": "16.8.3",
"react-native": "0.59.5",
"react-native-collapsible": "^1.4.0",
"react-native-gesture-handler": "^1.1.0",
"react-native-linear-gradient": "^2.5.4",
"react-native-masked-text": "^1.12.2",
"react-native-modalbox": "^1.7.1",
"react-native-paper": "^2.15.2",
"react-native-qrcode": "^0.2.7",
"react-native-slider": "^0.11.0",
"react-native-swiper": "^1.5.14",
"react-native-vector-icons": "^6.4.2",
"react-navigation": "^3.9.1",
"react-navigation-material-bottom-tabs": "^1.0.0"

```

## 🚀 ScreensShots

## 👏 Todo (Desenvolvimento)

- [x] Criar repositório no Github
- [x] Criar SplashScreen (Android)
- [x] Desenvolver Tela Pré-load (check User Active)

* Desenvolver Tela de Login
  - [x] Login Google?
  - [x] Login Facebook?
  - [x] Login Email e Senha
  - [x] Login pelo Cartão
* Desenvolver Tela Home
  - [x] Home Crédito fatura fechada
  - [x] Home Credito fatura atrasada
  - [x] Home Crédito
  - [x] Home Débito
  - [x] Home Pontos
  - [x] Home Clube do Carimbo
    - Home Dropdown
      - [x] Lojas
      - [x] Transações
      - [x] Faturas
      - [x] Faturas Pagas
      - [x] Faturas Atrasadas
      - [x] Prancheta ?
* Desenvolver Tela Clube de Vantagens
  - [x] Desenvolver tela principal
  - [x] Serviços
* Desenvolver Tela Prémios
  - [x] Tela Prémios
  - [x] Resgatar
  - [x] Dialog ?
  - [x] Resgate Erro

- [x] Desenvolver Tela Sorteios
- [x] Desenvolver Tela Cartão Virtual

* Menu Drawer
  - [x] Desenvolver Menu (Perfil, Ajuda, Notificações, Termos de Uso, Mensagens
    - [x] Perfil
      - [x] Editar Perfil
      - [x] Editar Erro
      - [x] Salvar
    - [ ] Notificações ???
    - [x] Termos de Uso
    - [x] Mensagens
    - [x] Ajuda

## How to version

Versionamento será dividido entre

- Mudanças significativas de funcionalidade do App (+x.0.0)
- Adição de novas funcionalidades (0.+x.0)
- Ajustes de Bugs (0.0.+x)

#### Exemplo:

> Foram adicionadas 3 novas telas, 5 novas funcionalidades e corrigidos 15 bugs. Logo a versão continuará 1, porém com 8 incrementos de funcionalidades e 15 correções de bugs. Versão final: 1.8.15

#### 👏 Todo (README.MD)

- [ ] Implementar ScreensShots no README.MD
- [ ] Adicionar Dependências
- [ ] Incrementar Todo(Dev)
